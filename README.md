# JAVA 
- Spring
- Lombok
- JPA e Hibernate
- Mysql
- Html

# APIs disponíveis: 
### lista todos ônibus
/api/api_linhas 

### salva dados da api exposta no banco de dados
/api/save_api_linhas

### filta ônibus por nome
api/buscapornome/{name}

### filtra rotas de ônibus
/busca_rotas/{id}

# Conexão com front/CRUD:
### create/ new
/new

### read/ home
/

### update/ edit
/edit/{id}

### delete
/delete/{id}

# Imagem do Site / CRUD
![](/CRUD.jpg)

