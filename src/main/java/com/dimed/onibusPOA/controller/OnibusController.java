package com.dimed.onibusPOA.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dimed.onibusPOA.model.Onibus;
import com.dimed.onibusPOA.service.OnibusService;

@Controller
public class OnibusController {
	@Autowired
	private OnibusService service;

	@RequestMapping("/")
	public String viewHomePage(Model model) {
		List<Onibus> listOnibus = service.listAll();

		model.addAttribute("listOnibus", listOnibus);

		return "index";
	}

	@RequestMapping("/new")
	public String showNewOnibusForm(Model model) {

		Onibus onibus = new Onibus();

		model.addAttribute("onibus", onibus);

		return "new_onibus";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveOnibus(@ModelAttribute("onibus") Onibus onibus) {
		service.save(onibus);

		return "redirect:/";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditOnibusForm(@PathVariable(name = "id") Long id) {
		ModelAndView mav = new ModelAndView("edit_onibus");
		
		Onibus onibus = service.get(id);
		mav.addObject("onibus", onibus);
		
		return mav;
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteOnibus(@PathVariable(name = "id") Long id) {
		service.delete(id);
		
		return "redirect:/";
	}
}
























