package com.dimed.onibusPOA.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dimed.onibusPOA.model.Onibus;

public interface OnibusRepository extends JpaRepository<Onibus, Long>{
	
}
