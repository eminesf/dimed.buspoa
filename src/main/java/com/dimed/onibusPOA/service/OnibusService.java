package com.dimed.onibusPOA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dimed.onibusPOA.model.Onibus;
import com.dimed.onibusPOA.repository.OnibusRepository;

@Service
public class OnibusService {
	@Autowired
	private OnibusRepository onibusRepository;
	
	public List<Onibus> listAll(){
		return onibusRepository.findAll();
	}
	
	public void save(Onibus onibus) {
		onibusRepository.save(onibus);
	}
	
	public Onibus get(Long id) {
		return onibusRepository.findById(id).get();
	}
	
	public void delete(Long id) {
		onibusRepository.deleteById(id);
	}
	
}
