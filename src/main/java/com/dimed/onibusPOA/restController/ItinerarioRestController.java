package com.dimed.onibusPOA.restController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.dimed.onibusPOA.model.Itinerario;
import com.dimed.onibusPOA.model.Rota;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/busca_rotas")
public class ItinerarioRestController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/{id}")
	public ResponseEntity<Itinerario> getItinerario(@PathVariable String id) throws JsonMappingException, JsonProcessingException{
		
		List<Rota> rota = new ArrayList<>();
		String url = "http://www.poatransporte.com.br/php/facades/process.php?a=il&p=";
		ResponseEntity<String> entity = restTemplate.getForEntity(url + id, String.class);
		
		ObjectMapper mapper = new ObjectMapper(); 
		JsonNode root = null;
		
		try {
			root = mapper.readTree(entity.getBody());
			Integer posicaoId = 0;
			
			while(true) {
				JsonNode posicao = root.path(""+posicaoId);
				if(posicao.isMissingNode()) {
					System.out.println(posicao.isMissingNode() + " " + posicaoId);
					break;
				}
				
				JsonNode lat = posicao.path("lat");
				JsonNode lng = posicao.path("lng");
				rota.add(new Rota(posicaoId.toString(),lat.asText(), lng.asText()));
				posicaoId++; 
	
			}
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		
		JsonNode nome = root.path("nome");
		JsonNode idlinha = root.path("idlinha");
		JsonNode codigo = root.path("codigo");
		
		Itinerario itinerario = new Itinerario(nome.asText(), idlinha.asText(), codigo.asText(), rota);
		
		
		
		return ResponseEntity.ok(itinerario);
	}

}
