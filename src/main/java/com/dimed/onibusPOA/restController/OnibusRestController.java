package com.dimed.onibusPOA.restController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.dimed.onibusPOA.model.Onibus;
import com.dimed.onibusPOA.service.OnibusService;

@RestController
@RequestMapping("/api")
public class OnibusRestController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private OnibusService service;
	
	@RequestMapping(value = "api_linhas", method = RequestMethod.GET, produces = "application/json")
	public List<Onibus> getAllOnibusFromApi(){
		
		String url = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";
		
		Onibus[] listOnibus = restTemplate.getForObject(url, Onibus[].class);
		
		return Arrays.asList(listOnibus);
	}
	
	@RequestMapping(value = "save_api_linhas", method = RequestMethod.POST, produces = "application/json")
	public void saveInDataBase() {
		for(Onibus onibus : getAllOnibusFromApi()) {
			service.save(onibus);
		}
	}
	
	@RequestMapping(value = "/buscapornome/{name}", method = RequestMethod.GET, produces = "application/json")
	public List<Onibus> getOnibusByName(@PathVariable String name) {

		List<Onibus> allBusByName = new ArrayList<Onibus>();
		List<Onibus> listOnibus = getAllOnibusFromApi();

		for (Onibus onibus : listOnibus) {
			if (onibus.getNome().toLowerCase().contains(name.toLowerCase())) {
				allBusByName.add(onibus);
			}
		}
		return allBusByName;
	}
	
}
