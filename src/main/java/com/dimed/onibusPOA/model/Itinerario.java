package com.dimed.onibusPOA.model;

import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Itinerario {
	
	private String idlinha;
	private String nome;
	private String codigo;
	private List<Rota> rota;

}
