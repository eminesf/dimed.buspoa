package com.dimed.onibusPOA.model;

import javax.persistence.Entity;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Onibus {
	
	@Setter
	private Long id;
	@Setter @Getter
	private String codigo;
	@Setter @Getter
	private String nome;	
	
	@Id
	public Long getId() {
		return id;
	}
}
