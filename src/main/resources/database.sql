CREATE DATABASE onibuspoa;

CREATE TABLE `onibuspoa`.`onibus` (
  `id` INT NOT NULL,
  `codigo` VARCHAR(45) NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));
